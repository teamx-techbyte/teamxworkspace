const mongoose = require("mongoose");
const jobDecriptionSchema = new mongoose.Schema({
    jobProfile: String,
    jobDescription: String,
    technichalSkills: String,
    softSkills: String,
    experience: String,
    education: String,
    jobType: String,
    jobLocation: String,
    isDeleted: Boolean,
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret, option) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.password;
            delete ret.__v;
            return ret;
        }
    }
})
module.exports = mongoose.model("jobDecription", jobDecriptionSchema)