const mongoose = require("mongoose");
const candidateProfileSchema = new mongoose.Schema({
   candidate:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'candidateProfile'
    },
   matchPercentage: {type: Number} 

}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret, option) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.password;
            delete ret.__v;
            return ret;
        }
    }
})
module.exports = mongoose.model("candidateProfileMatch", candidateProfileSchema)