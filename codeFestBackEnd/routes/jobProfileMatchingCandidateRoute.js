const exporess = require("express");
const jobProfileMatchingCandidate = require("../controller/jobProfileMatchingCandidate");
const router =  exporess.Router();
const tokenValidation =  require("../middleware/tokenValidationJwt");


//----- Get matching candidates by job id -----------
router.get("/:id",
jobProfileMatchingCandidate.getById);


module.exports = router;