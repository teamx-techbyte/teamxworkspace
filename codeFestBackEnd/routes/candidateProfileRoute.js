const exporess = require("express");
const candidateProfile = require("../controller/candidateProfile");
const router =  exporess.Router();
const tokenValidation =  require("../middleware/tokenValidationJwt");

//----- Create -----------
router.post("/", 
candidateProfile.create
);

//----- Get Client by id -----------
router.get("/:id",
candidateProfile.getById);

//----- Update -----------
router.put("/:id",
candidateProfile.update);


//----- Get all List -----------
router.get("/",
candidateProfile.get
);

//----- Delete -----------
router.delete("/:id",
candidateProfile.delete
);
module.exports = router;