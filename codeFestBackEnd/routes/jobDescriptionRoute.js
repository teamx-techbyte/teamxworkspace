const exporess = require("express");
const jobDescription = require("../controller/jobDescription");
const router =  exporess.Router();
const tokenValidation =  require("../middleware/tokenValidationJwt");

//----- Create -----------
router.post("/", 
jobDescription.create
);

//----- Get Client by id -----------
router.get("/:id",
jobDescription.getById);

//----- Update -----------
router.put("/:id",
jobDescription.update);


//----- Get all List -----------
router.get("/",
jobDescription.get
);

//----- Delete -----------
router.delete("/:id",
jobDescription.delete
);
module.exports = router;