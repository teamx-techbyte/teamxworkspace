
const constant = require("../constant")
const CandidateProfileModel = require("../database/models/candidateProfileModel")
const { formatMongoData } = require("../helper/dbhelper")
const mongoose = require("mongoose")

module.exports.create = async (serviceData) => {
  try {
    let candidateProfileModel = new CandidateProfileModel({ ...serviceData })
    let result = await candidateProfileModel.save();
    return formatMongoData(result)
  } catch (err) {
    console.log("something went wrong: service > candidateProfileModel ", err);
    throw new Error(err)
  }
}


module.exports.get = async ({ skip = 0, limit = 10 }) => {
  try {
    let candidateProfileModel = await CandidateProfileModel.find().skip(parseInt(skip)).limit(parseInt(limit));

    return formatMongoData(candidateProfileModel)
  } catch (err) {
    console.log("something went wrong: service > candidateProfileModel ", err);
    throw new Error(err)
  }
}

module.exports.getById = async ({ id }) => {
  try {
    if (!mongoose.Types.ObjectId(id)) {
      throw new Error(constant.candidateProfile.INVALID_ID)
    }
    let candidateProfileModel = await CandidateProfileModel.findById(id);
    if (!candidateProfileModel) {
      throw new Error(constant.candidateProfile.CANDIDATE_PROFILE_NOT_FOUND)
    }
    return formatMongoData(candidateProfileModel)
  } catch (err) {
    console.log("something went wrong: service > candidateProfileModel ", err);
    throw new Error(err)
  }
}

module.exports.update = async ({ id, updateInfo }) => {
  try {

    let candidateProfileModel = await CandidateProfileModel.findOneAndUpdate({ _id: id }, updateInfo, { new: true });
    if (!candidateProfileModel) {
      throw new Error(constant.candidateProfile.CANDIDATE_PROFILE_NOT_FOUND)
    }
    return formatMongoData(candidateProfileModel)
  } catch (err) {
    console.log("something went wrong: service > candidateProfileModel ", err);
    throw new Error(err)
  }
}


module.exports.delete = async ({ id }) => {
  try {

    let candidateProfileModel = await CandidateProfileModel.updateOne({ _id: id }, { isDeleted: true });

    if (!candidateProfileModel) {
      throw new Error(constant.candidateProfile.CANDIDATE_PROFILE_NOT_FOUND)
    }
    return formatMongoData(candidateProfileModel)
  } catch (err) {
    console.log("something went wrong: service > candidateProfileModel", err);
    throw new Error(err)
  }
}