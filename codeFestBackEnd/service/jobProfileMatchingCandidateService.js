
const constant = require("../constant")
const CandidateProfileModel = require("../database/models/candidateProfileModel")
const JobDecriptionModel = require("../database/models/jobDecriptionModel")
const { formatMongoData } = require("../helper/dbhelper")
const mongoose = require("mongoose")
const { count } = require("../database/models/candidateProfileModel")
const CandidateProfileMatchModel = require("../database/models/candidateProfileMatchingModel")



module.exports.getById = async ({ id }) => {
  try {
    if (!mongoose.Types.ObjectId(id)) {
      throw new Error(constant.jobDescriptionMessage.INVALID_ID)
    }
    //get job description by job ID

    let jobDecriptionModel = await JobDecriptionModel.findById(id);
    if (!jobDecriptionModel) {
      throw new Error(constant.jobDescriptionMessage.JOB_DESCRIPTION_NOT_FOUND)
    }
    let techSkillJD = jobDecriptionModel.technichalSkills; // java,sprin,database
    //get All Candidate profiles
    let candidateProfileModel = await CandidateProfileModel.find();

    var matchingProfileList = [{matchingCandidates: {},matchPersent: ""}];

    // match candidate tech skill from JD
     
    candidateProfileModel.forEach((candidate, index) => {
      let techSkillCandidate = candidate.technichalSkills;

      const techSkillCandidateArr = techSkillCandidate.split(',');
      let count =0;
      techSkillCandidateArr.map(skill =>{
        if(techSkillJD.includes(skill)){
          count++
        }
      }) 

      let techSkillPer = count/(techSkillJD.split(',').length)*100;
      matchingProfileList.push({...candidate},techSkillPer); 
    });

    if (!matchingProfileList) {
      throw new Error(constant.candidateProfile.CANDIDATE_PROFILE_NOT_FOUND)
    }

    return formatMongoData(candidateProfileModel)

  } catch (err) {
    console.log("something went wrong: service > candidateProfileModel ", err);
    throw new Error(err)
  }

  
}




