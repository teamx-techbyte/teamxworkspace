
const constant = require("../constant");
const userModel = require("../database/models/userModel");
const {formatMongoData} = require("../helper/dbhelper");
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const dotEnv = require("dotenv");
let envName = "";
dotEnv.config();
module.exports.signup = async({email,password})=>{
    try{
     const user = await userModel.findOne({email});
     if(user){
         throw new Error(constant.userMessage.DUPLICATE_EMAIL);
     }
     password = await bcrypt.hash(password,12);
     const newUser = new userModel({email,password});
     let result = await newUser.save();
     return formatMongoData(result)
    }catch(err){
      console.log("something went wrong: service > signup ", err);
      throw new Error(err)
    }
  }
  
  module.exports.login = async(req)=>{
    try{
      const headresEmailAndpassword = req.headers.authorization.split('Basic ')[1].replace('"','');
      const emailNpassword = Buffer.from(headresEmailAndpassword, 'base64').toString().split(":");
      const email = emailNpassword[0].trim();
      const password = emailNpassword[1].trim();
     const user = await userModel.findOne({email});
     if(!user){
         throw new Error(constant.userMessage.USER_NOT_FOUND);
     }
     const isValid = await bcrypt.compare(password, user.password);
     if(!isValid){
        throw new Error(constant.userMessage.INVALID_password);
     }
     var token = jwt.sign({ id: user._id },process.env.SECRET_KEY || "codefest-teamx#$#", {expiresIn:"1d"});
     return {token};
    }catch(err){
      console.log("something went wrong: service > login ", err);
      throw new Error(err)
    }
  }