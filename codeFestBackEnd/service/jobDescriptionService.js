
const constant = require("../constant")
const JobDecriptionModel = require("../database/models/jobDecriptionModel")
const { formatMongoData } = require("../helper/dbhelper")
const mongoose = require("mongoose")

module.exports.create = async (serviceData) => {
  try {
    let jobDecriptionModel = new JobDecriptionModel({ ...serviceData })
    let result = await jobDecriptionModel.save();
    return formatMongoData(result)
  } catch (err) {
    console.log("something went wrong: service > JobDecription ", err);
    throw new Error(err)
  }
}


module.exports.get = async ({ skip = 0, limit = 10 }) => {
  try {
    let jobDecriptionModel = await JobDecriptionModel.find().skip(parseInt(skip)).limit(parseInt(limit));

    return formatMongoData(jobDecriptionModel)
  } catch (err) {
    console.log("something went wrong: service > JobDecription ", err);
    throw new Error(err)
  }
}

module.exports.getById = async ({ id }) => {
  try {
    if (!mongoose.Types.ObjectId(id)) {
      throw new Error(constant.jobDescriptionMessage.INVALID_ID)
    }
    let jobDecriptionModel = await JobDecriptionModel.findById(id);
    if (!jobDecriptionModel) {
      throw new Error(constant.jobDescriptionMessage.JOB_DESCRIPTION_NOT_FOUND)
    }
    return formatMongoData(jobDecriptionModel)
  } catch (err) {
    console.log("something went wrong: service > JobDecription ", err);
    throw new Error(err)
  }
}

module.exports.update = async ({ id, updateInfo }) => {
  try {

    let jobDecriptionModel = await JobDecriptionModel.findOneAndUpdate({ _id: id }, updateInfo, { new: true });
    if (!jobDecriptionModel) {
      throw new Error(constant.jobDescriptionMessage.JOB_DESCRIPTION_NOT_FOUND)
    }
    return formatMongoData(jobDecriptionModel)
  } catch (err) {
    console.log("something went wrong: service > JobDecription ", err);
    throw new Error(err)
  }
}


module.exports.delete = async ({ id }) => {
  try {

    let jobDecriptionModel = await JobDecriptionModel.updateOne({ _id: id }, { isDeleted: true });

    if (!jobDecriptionModel) {
      throw new Error(constant.jobDescriptionMessage.JOB_DESCRIPTION_NOT_FOUND)
    }
    return formatMongoData(jobDecriptionModel)
  } catch (err) {
    console.log("something went wrong: service > JobDecription ", err);
    throw new Error(err)
  }
}