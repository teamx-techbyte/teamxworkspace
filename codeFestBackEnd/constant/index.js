module.exports = {
    defaultResponce: {
        status: 400,
        message: "",
        body: {}
    },
    userMessage: {
        SIGNUP_SUCCESS: "Signup successfully",
        DUPLICATE_EMAIL: "User alrady exsist with given email",
        LOGIN_SUCCESS: "Login successfully",
        USER_NOT_FOUND: "User not found",
        INVALID_PASS: "Invalid password"
    },
    candidateProfile: {
        CANDIDATE_PROFILE_CREATED: "Candidate Profile  created successfully",
        CANDIDATE_PROFILE_ERROR: "Candidate Profile create fail",
        CANDIDATE_PROFILE_FETCH: "All Candidate Profile fetch successfully",
        CANDIDATE_PROFILE_GET_ERROR: "All Candidate Profile fetch error",
        CANDIDATE_PROFILE_NOT_FOUND: "Candidate Profile not found",
        INVALID_ID: "Invalid id",
        CANDIDATE_PROFILE_UPDATE: "Candidate Profile update successfully",
        CANDIDATE_PROFILE_DELETE: "Candidate Profile delete successfully",
    },
    jobDescriptionMessage: {
        JOB_DESCRIPTION_CREATED: "Job description created successfully",
        JOB_DESCRIPTION_ERROR: "Job description create fail",
        JOB_DESCRIPTION_FETCH: "All Job descriptions fetch successfully",
        JOB_DESCRIPTION_GET_ERROR: "All Job description fetch error",
        JOB_DESCRIPTION_NOT_FOUND: "Job description not found",
        INVALID_ID: "Invalid id",
        JOB_DESCRIPTION_UPDATE: "Job description update successfully",
        JOB_DESCRIPTION_DELETE: "Job description delete successfully",
    },
    jobProfileMatchingCandidateMessage: {
        JOB_DESCRIPTION_MATCHING_PROFILE_FETCH: "All matching candidates fetch successfully",
        JOB_DESCRIPTION_GET_ERROR: "All Job description fetch error",
        JOB_DESCRIPTION_NOT_FOUND: "Job description not found",
        INVALID_ID: "Invalid id",
    },
    requestValidationMessage: {
        BAD_REQUEST: "Invalid filds",
        TOKEN_MISSING: "Token missing from header",
        AUTHORIZATION_MISSING: "Authorization missing from header",
    },
    roleMessage: {
        ROLE_CREATED: "Role created successfully",
        ROLE_ERROR: "Role create fail",
        ROLE_FETCH: "All Roles fetch successfully",
        ROLE_GET_ERROR: "All Role featch error",
        ROLE_NOT_FOUND: "Role not found",
        INVALID_ID: "Invalid id",
        ROLE_UPDATE: "Role update successfully",
        ROLE_DELETE: "Role delete successfully",
    },
}