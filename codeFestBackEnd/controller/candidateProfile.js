const candidateProfileService  =  require("../service/candidateProfileService");
const constant =  require("../constant");

module.exports.create = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceCandidateProfile = await candidateProfileService.create(req.body);
        responce.status = 200;
        responce.message = constant.candidateProfile.CANDIDATE_PROFILE_CREATED;
        responce.body = responceCandidateProfile;
    }catch(err){
        responce.message = err.message;
    }

    return res.status(responce.status).send(responce);
}

module.exports.getById = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceCandidateProfile = await candidateProfileService.getById(req.params);
        responce.status = 200;
        responce.message = constant.candidateProfile.CANDIDATE_PROFILE_FETCH;
        responce.body = responceCandidateProfile;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}

module.exports.get = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceCandidateProfile = await candidateProfileService.get(req.query);
        responce.status = 200;
        responce.message = constant.candidateProfile.CANDIDATE_PROFILE_FETCH;
        responce.body = responceCandidateProfile;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}

module.exports.update = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceCandidateProfile = await candidateProfileService.update({
            id:req.params.id,
            updateInfo:req.body,
        });
        responce.status = 200;
        responce.message = constant.candidateProfile.CANDIDATE_PROFILE_UPDATE;
        responce.body = responceCandidateProfile;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}

module.exports.delete = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
       const responceCandidateProfile = await candidateProfileService.delete(req.params);
        responce.status = 200;
        responce.message = constant.candidateProfile.CANDIDATE_PROFILE_DELETE;
        responce.body = responceCandidateProfile;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}


