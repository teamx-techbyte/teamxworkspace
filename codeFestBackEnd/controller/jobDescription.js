const jobDescriptionService  =  require("../service/jobDescriptionService");
const constant =  require("../constant");

module.exports.create = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceJobDescription = await jobDescriptionService.create(req.body);
        responce.status = 200;
        responce.message = constant.jobDescriptionMessage.JOB_DESCRIPTION_CREATED;
        responce.body = responceJobDescription;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}

module.exports.getById = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceJobDescription = await jobDescriptionService.getById(req.params);
        responce.status = 200;
        responce.message = constant.jobDescriptionMessage.JOB_DESCRIPTION_FETCH;
        responce.body = responceJobDescription;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}

module.exports.get = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceJobDescription = await jobDescriptionService.get(req.query);
        responce.status = 200;
        responce.message = constant.jobDescriptionMessage.JOB_DESCRIPTION_FETCH;
        responce.body = responceJobDescription;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}

module.exports.update = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceJobDescription = await jobDescriptionService.update({
            id:req.params.id,
            updateInfo:req.body,
        });
        responce.status = 200;
        responce.message = constant.jobDescriptionMessage.JOB_DESCRIPTION_UPDATE;
        responce.body = responceJobDescription;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}

module.exports.delete = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
       const responceJobDescription = await jobDescriptionService.delete(req.params);
        responce.status = 200;
        responce.message = constant.jobDescriptionMessage.JOB_DESCRIPTION_DELETE;
        responce.body = responceJobDescription;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}


