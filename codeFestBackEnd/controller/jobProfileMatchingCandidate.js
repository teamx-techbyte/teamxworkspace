const jobProfileMatchingCandidateService  =  require("../service/jobProfileMatchingCandidateService");
const constant =  require("../constant");

module.exports.getById = async (req,res)=>{
    let responce = {...constant.defaultResponce};
    try{
        const responceCandidateProfile = await jobProfileMatchingCandidateService.getById(req.params);
        responce.status = 200;
        responce.message = constant.candidateProfile.JOB_DESCRIPTION_MATCHING_PROFILE_FETCH;
        responce.body = responceCandidateProfile;
    }catch(err){
        responce.message = err.message;
    }
    return res.status(responce.status).send(responce);
}









